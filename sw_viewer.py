import pandas as pd
import json
import os
import io
import sys
sys.path.append('lib')
from sw_data_tools import ace
from sw_data_tools import dscovr
from sw_data_tools import kp_gfz
from sw_data_tools import swpc_geomag_forecast
from sw_data_tools import swpc_realtime_sw
from sw_data_tools import basictools
import requests
from flask import Flask, send_file
app = Flask(__name__)

@app.route("/") 
def root():
    return app.send_static_file('html/sw_viewer.html')

@app.route("/favicon.ico") 
def favicon():
    return app.send_static_file('img/favicon.ico')


@app.route("/sdo/img/dailymov/<path:moviesubpath>")
def sdodailymovie(moviesubpath):
    localpath = f"static/data/sdo/img/dailymov/{moviesubpath}"
    print(localpath)
    if os.path.isfile(localpath):
        print("Movie already exists")
    else:
        print("Should download movie here from SDO website")
        remotepath = f"https://sdo.gsfc.nasa.gov/assets/img/dailymov/{moviesubpath}"
        print("Downloading: " + remotepath + " to " + os.path.dirname(localpath))
        basictools.ensure_data_dir(os.path.dirname(localpath))
        r = requests.get(remotepath, stream=True)
        if r.status_code == 200:
            with open(localpath, 'wb') as f:
                for chunk in r:
                    f.write(chunk)
        r.close()
    
    return send_file(localpath, mimetype='video/mp4')

@app.route("/kp/<startdate>/<enddate>/<resample_interval>")
def kp(startdate, enddate, resample_interval):
    t0 = pd.to_datetime(startdate, utc=True)
    t1 = pd.to_datetime(enddate, utc=True)
    dfkp = kp_gfz.timeinterval_to_pandas(t0, t1)
    if resample_interval == '3H':
        return dfkp.to_csv(index=True, date_format='%Y-%m-%d %H:%M:%S')    	
    else:
        dfkp_resampled = dfkp.resample(resample_interval).max()
        return dfkp_resampled.to_csv(index=True, date_format='%Y-%m-%d %H:%M:%S')

@app.route("/ace_mag/<startdate>/<enddate>/<resample_interval>")
def ace_mag(startdate, enddate, resample_interval):
    t0 = pd.to_datetime(startdate, utc=True)
    t1 = pd.to_datetime(enddate, utc=True)
    dfimf = ace.mag_timeinterval_to_pandas(t0, t1)
    if resample_interval == '':
        return dfimf.to_csv(index=True, date_format='%Y-%m-%d %H:%M:%S')
    else:
        dfimf_resampled = dfimf.resample(resample_interval).max()
        return dfimf_resampled.to_csv(index=True, date_format='%Y-%m-%d %H:%M:%S')


@app.route("/ace_swepam/<startdate>/<enddate>/<resample_interval>")
def ace_swepam(startdate, enddate, resample_interval):
    t0 = pd.to_datetime(startdate, utc=True)
    t1 = pd.to_datetime(enddate, utc=True)
    dfswepam = ace.swepam_timeinterval_to_pandas(t0, t1)
    if resample_interval == '':
        return dfswepam.to_csv(index=True, date_format='%Y-%m-%d %H:%M:%S')
    else:
        dfswepam_resampled = dfswepam.resample(resample_interval).max()
        return dfswepam_resampled.to_csv(index=True, date_format='%Y-%m-%d %H:%M:%S') 


@app.route("/kp_swpc_geomag_forecast")
def kp_forecast():
    dfkp = swpc_geomag_forecast.kp_geomagnetic_forecast()
    return dfkp.to_csv(index=True, date_format='%Y-%m-%d %H:%M:%S')

@app.route("/mag-7-day/<startdate>/<enddate>/<resample_interval>")
def mag_7_day(startdate,enddate,resample_interval):
    t0 = pd.to_datetime(startdate, utc=True)
    t1 = pd.to_datetime(enddate, utc=True)
    dfimf = swpc_realtime_sw.mag()[t0:t1]
    dfimf_resampled = dfimf.resample(resample_interval).mean()
    return dfimf_resampled.to_csv(index=True, date_format='%Y-%m-%d %H:%M:%S')

@app.route("/plasma-7-day/<startdate>/<enddate>/<resample_interval>")
def plasma_7_day(startdate,enddate,resample_interval):
    t0 = pd.to_datetime(startdate, utc=True)
    t1 = pd.to_datetime(enddate, utc=True)
    dfimf = swpc_realtime_sw.plasma()[t0:t1]
    dfimf_resampled = dfimf.resample(resample_interval).mean()
    return dfimf_resampled.to_csv(index=True, date_format='%Y-%m-%d %H:%M:%S')

if __name__ == "__main__":
    app.run(port=8000, debug=True)
