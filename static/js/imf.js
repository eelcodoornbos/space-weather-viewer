// global xaxis object -> determines level of zoom, on zoom, updates all subplots


function plot_element_imf() {
  // plot element object -> one specific for Kp, one for IMF bt, one for IMF bz, etc., 
  // provides setup and update methods
  // could be reused with different settings (for data source, style properties)

  var axes, ytransform, plotgroup, imfdata, subplot;

  var plotelement = {};

  var imfdata;

  plotelement.subplot = function(value) {
    if (!arguments.length) return subplot;
    subplot = value;
    ytransform = subplot.ytransform();
    plotgroup = subplot.plotgroup();
    return plotelement;
  }

  plotelement.loadData = function() {

    function formatDate(date) {
      return date.toISOString().slice(0, 19);
    } 

    function datawidth(xtransform, duration) {
      return xtransform(duration)-xtransform(0);  
    }

   function transformData(d) {
      // d.bz_gsm = d.Bz;
      // d.bt = d.Bt;
      return d;
    }

    t0 = new Date(globalx.xtransform_z.domain()[0].getTime() - 180*1e3); // include 3 mins previously to avoid empty first bar
    t1 = new Date(globalx.xtransform_z.domain()[1].getTime() + 180*1e3); 

    minpixels = 1;
    if (datawidth(globalx.xtransform_z, 60*1e3) > minpixels) {
      subsample_time = '1T';
    } else if (datawidth(globalx.xtransform_z, 3*60*1e3) > minpixels) {
      subsample_time = '3T';      
    } else if (datawidth(globalx.xtransform_z, 5*60*1e3) > minpixels) {
      subsample_time = '5T';
    } else if (datawidth(globalx.xtransform_z, 10*60*1e3) > minpixels) {
      subsample_time = '10T';
    } else if (datawidth(globalx.xtransform_z, 20*60*1e3) > minpixels) {
      subsample_time = '20T';          
    } else if (datawidth(globalx.xtransform_z, 30*60*1e3) > minpixels) {
      subsample_time = '30T';                
    } else if (datawidth(globalx.xtransform_z, 3600*1e3) > minpixels) {
      subsample_time = '1H';    
    } else {
      subsample_time = '3H';
    }
    url = '/mag-7-day/' + formatDate(t0) + "/" + formatDate(t1) + '/' + subsample_time;
    // url = '/ace_mag/' + formatDate(t0) + "/" + formatDate(t1) + '/' + subsample_time;
    d3.csv(url, transformData, function(error, data) {
      if (error) throw error;
      imfdata = data;
      plotelement.setup();
      plotelement.update();
    });
  }

  plotelement.setup = function() {
    plotgroup.selectAll(".Bzline")
        .remove();

    plotgroup.selectAll(".Btline")
        .remove();

    // plotgroup.data(imfdata)

    var Bzline = d3.area()
      .x(function(d) { return globalx.xtransform_z(parseDate(d.time_tag)); })
      .y0(ytransform  (0))
      .y1(function(d) { return ytransform(d.bz_gsm); })    


    var Btline = d3.area()
      .x(function(d) { return globalx.xtransform_z(parseDate(d.time_tag)); })
      .y0(function(d) { return ytransform(-d.bt); })
      .y1(function(d) { return ytransform(d.bt); })    

    plotgroup.append("path")
      .attr("class", "Btline")
      .attr("clip-path", "url(#" +  plotelement.subplot().id() + "_clip)")

    plotgroup.append("path")
      .attr("class", "Bzline")
      .attr("clip-path", "url(#" +  plotelement.subplot().id() + "_clip)")
  
    return plotelement;
  }

  plotelement.update = function() {
    var Bzline = d3.area()
      .x(function(d) { return globalx.xtransform_z(parseDate(d.time_tag)); })
      .y0(ytransform  (0))
      .y1(function(d) { return ytransform(d.bz_gsm); })    

    plotgroup.selectAll(".Bzline")
      .datum(imfdata)    
      .attr("d", Bzline);

    var Btline = d3.area()
      .x(function(d) { return globalx.xtransform_z(parseDate(d.time_tag)); })
      .y0(function(d) { return ytransform(-d.bt); })
      .y1(function(d) { return ytransform(d.bt); })    

    plotgroup.selectAll(".Btline")
      .datum(imfdata)
      .attr("d", Btline);


    return plotelement;
  }

  plotelements.push(plotelement);
  return plotelement;
}
