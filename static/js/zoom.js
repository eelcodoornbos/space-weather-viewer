var now = new Date();
var tmaxzoom = d3.utcDay.offset(now, 180);

var parseDate = d3.utcParse("%Y-%m-%d %H:%M:%S");

var timer = null;

function perform_zoom() {
  // This function is called each time the user performs a zoom/pan action with the mouse
  //   (panning by dragging or zooming using the scroll wheel)
  var newx = d3.mouse(this)[0]; // Save the cursor x-location
   
  // Loop over each of the subplots (rows) to update their x-axes
  subplots.forEach(
    function(subplot, index) {
      subplot.setzoom(d3.event.transform);
      subplot.updateDateTimeAxis();
      subplot.setmouseindicator(newx);
    }
  );

  // Loop over each of the plot elements (e.g. GFZ Kp and NOAA predicted Kp are two plot elements connected to the same subplot)
  plotelements.forEach(
    function(plotelement, index) {
      plotelement.update();
    }
  );

  // Restart the timer...
  if(timer !== null) {
      clearTimeout(timer);        
  }
  // And if there are no new zoom events for 100 ms, the data that is displayed for each plotelement will be reloaded  
  timer = setTimeout(function() {
    plotelements.forEach(
      function(plotelement, index) {
        plotelement.loadData();
      })
  }, 100);    

}