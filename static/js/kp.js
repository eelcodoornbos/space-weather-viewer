// global xaxis object -> determines level of zoom, on zoom, updates all subplots


function plot_element_kp() {
  // plot element object -> one specific for Kp, one for IMF bt, one for IMF bz, etc., 
  // provides setup and update methods
  // could be reused with different settings (for data source, style properties)

  var axes, ytransform, plotgroup, kpdata, subplot;

  var plotelement = {};

  var kpduration;

  var datatype = "gfz";

  var barPadding = 0.2;

  var showkptext = true;



  plotelement.subplot = function(value) {
    if (!arguments.length) return subplot;
    subplot = value;
    ytransform = subplot.ytransform();
    plotgroup = subplot.plotgroup();
    return plotelement;
  }

  plotelement.datatype = function(value) {
    if (!arguments.length) return datatype;
    datatype = value;
    return plotelement;
  }

  plotelement.showkptext = function(value) {
    if (!arguments.length) return showkptext;
    showkptext = value;
    return plotelement;
  }

  plotelement.barpadding = function(value) {
    if (!arguments.length) return barPadding;
    barPadding = value;
    return plotelement;
  }

  function kpcolor(kp) {
  	// if (datatype === "forecast") { return "#A0A0A0"; }
    if (kp < 4.8) {return '#88CC66';} // green
    if (kp < 5.8) {return '#FFF033';} // yellow
    if (kp < 6.8) {return '#FFBB44';} // light orange 
    if (kp < 7.8) {return '#DD9922';} // dark orange
    if (kp < 8.8) {return '#FF2020';} // red
    if (kp < 9.8) {return '#A02020';} // dark red
  }

  function kptext(kp) {
    kpint = Math.trunc(kp)
    kpdif = kp-kpint;
    if (kpdif < 0.1) {
      return kpint.toFixed(0);
    }
    if (kpdif > 0.2 && kpdif < 0.4) {
      return kpint.toFixed(0) + '+';
    }
    if (kpdif > 0.6 && kpdif < 0.9) {
      return (kpint+1).toFixed(0) + '-';
    }
  }

  function kpwidth(kpduration) {
  	// Returns the width of the Kp interval in pixels
    return globalx.xtransform_z(kpduration)-globalx.xtransform_z(0);  
  }

  plotelement.loadData = function() {
    t0 = new Date(globalx.xtransform_z.domain()[0].getTime() - 50*3*3600*1e3); // include 3 hours previously to avoid empty first bar
    t1 = globalx.xtransform_z.domain()[1];// + 3*3600*1e3;

    minpixels = 2;
    if (kpwidth(3*3600*1e3) > minpixels) {
      subsample_time = '3H'; // default is 3 hours
    } else if (kpwidth(86400*1e3) > minpixels) {
      subsample_time = '1D'; // day
    } else if (kpwidth(4*86400*1e3) > minpixels) {
      subsample_time = '4D'; // day      
    } else if (kpwidth(7*86400*1e3) > minpixels) {
      subsample_time = 'W'; // week
    } else if (kpwidth(15*86400*1e3) > minpixels) {
      subsample_time = 'SM'; // semi-month      
    } else if (kpwidth(30*86400*1e3) > minpixels) {
      subsample_time = 'M'; // month start
    } else {
    	subsample_time = '3M'; // year
    }

	  function formatDate(date) {
	    return date.toISOString().slice(0, 19);
	  }     

		function transformData(d) {
	    d.date = parseDate(d.DateTime);
	    d.Kp = +d.Kp;
	    return d;
	  }

	  if (datatype === "forecast") {
			url = "/kp_swpc_geomag_forecast";
	  } else {
    	url = "/kp/" + formatDate(t0) + "/" + formatDate(t1) + '/' + subsample_time;
    }
    d3.csv(url, transformData, function(error, data) {
      if (error) throw error;
			kpduration = data[1].date - data[0].date;
      plotelement.setup(data);
      plotelement.update();
    });
  }

  plotelement.setup = function(kpdata) {
    plotgroup.selectAll(".kp_" + datatype)
    	// .exit()
      .remove();

    plotgroup.selectAll(".kp_" + datatype)
      .data(kpdata)
      .enter()
        .append("rect")
        .attr("class", "kp_" + datatype)
        // .attr("stroke", "black")
        .attr("fill", function(d) { return kpcolor(d.Kp); } )
        .attr("height", function(d) { return ytransform.range()[0]-ytransform(d.Kp); } )
        .attr("clip-path", "url(#" +  plotelement.subplot().id() + "_clip)");

    if (showkptext) {
       plotgroup.selectAll(".kptext" + "_" + datatype)
       	.data(kpdata)
       	.enter()
  	      .append("text")
  	      .attr("class", "kptext")
          .attr("clip-path", "url(#" +  plotelement.subplot().id() + "_clip)")
  	      .attr("fill", "black")// kptextcolor(d.Kp); } )
  	      .text(function(d) {return kptext(d.Kp)});
    } else {
       plotgroup.selectAll(".kptext" + "_" + datatype)
        .remove();
    }

    return plotelement;
  }

  plotelement.update = function() {
    // if (d3.event) {
    //   globalx.xtransform_z = d3.event.transform.rescaleX(globalx.xtransform);
    // } else {
    //   zoomedxtransform = globalx.xtransform;
    // }
    kp_width = globalx.xtransform_z(kpduration)-globalx.xtransform_z(0);

    var barwidth = kp_width * (1-barPadding);
    var baroffset = kp_width * barPadding/2;

    plotgroup.selectAll(".kp_" + datatype)
      .attr("x", function(d) { return globalx.xtransform_z(d.date)+baroffset;} )
      .attr("y", function(d) { return ytransform(d.Kp); })
      .attr("width", barwidth);

    if (showkptext) {
      if (barwidth > 25) {
        plotgroup.selectAll(".kptext")
            .attr("x", function(d) { return globalx.xtransform_z(d.date)+barwidth/2+baroffset;} )
            .attr("y", function(d) { return ytransform(d.Kp) - 4; })
            .style("visibility", "visible");
        // plotgroup.selectAll(".kp_" + datatype)
        //   .attr("stroke", "black")
        //   .attr("stroke-width", "1");
      } else {
        plotgroup.selectAll(".kptext")
          .style("visibility", "hidden");
        // plotgroup.selectAll(".kp_" + datatype)
        //   .attr("stroke", "none");
      }
    }

    return plotelement;
  }

  plotelements.push(plotelement);
  return plotelement;
}