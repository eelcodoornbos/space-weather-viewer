#!/usr/bin/env python3
import basictools
import os

weekly_files_original = [
    "27DO.txt",
    "Predict.txt",
    "RecentIndices.txt",
    "WKHF.txt",
    "scycle.gif",
    "sunspot.gif",
    "f10.gif",
    "Ap.gif"
]

def download_weekly_files():
    '''Download weekly data files from the SWPC ftp server and store them locally'''
    import ftplib
    localdir = basictools.ensure_data_dir('swpc_weekly')
    ftp_host = 'ftp.swpc.noaa.gov'
    ftp_dir = '/pub/weekly'
    ftp_connection = ftplib.FTP(ftp_host)
    ftp_connection.login()
    ftp_connection.cwd(ftp_dir)
    for ftp_filename in weekly_files_original:
        ftp_localfilename = f'{localdir}/{ftp_filename}'
        print(f"Attempting to download {ftp_localfilename}")
        try:
            ftp_connection.retrbinary("RETR " + ftp_filename, open(ftp_localfilename, 'wb').write)
        except:
            print(f"Error trying to download file {ftp_filename}")
            os.remove(ftp_localfilename)
    ftp_connection.quit()
#    if not os.path.isfile(ftp_localfilename):

download_weekly_files()
