import os
import re
from glob import glob
import numpy as np
import pandas as pd
from . import netcdftools

STORAGE_LOCATION = '.'

def get_filelist(datatype, t0, t1):
    import requests
    t0str = t0.strftime('%Y-%m-%dT%H:%MZ')
    t1str = t1.strftime('%Y-%m-%dT%H:%MZ')
    url = f'https://www.ngdc.noaa.gov/next-catalogs/rest/dscovr/catalog/filename?processEnvs=oe&dataTypes={datatype}&dataStartTime={t0str}&dataEndTime={t1str}'
    r = requests.get(url)
    return r.json()['items']


def download(datatype, t0, t1):
    import requests
    data_dir = ensure_data_dir()
    filelist = get_filelist(datatype, t0, t1)
    for filename in filelist:
        pathname = f'{data_dir}/{filename}'
        if not os.path.isfile(pathname):
            #oe_m1m_dscovr_s20161007000000
            year = filename[15:19]
            month = filename[19:21]
            url = f'https://www.ngdc.noaa.gov/dscovr/data/{year}/{month}/{filename}'
            print("getting " + url)
            r = requests.get(url)
            if r.status_code == 200:
                with open(pathname, 'wb') as f:
                    f.write(r.content)
            else:
                print(f"Getting {url} resulted in status code: {r.status_code}")
            
    
def match_file(filepath):
    """Parses the DSCOVR data format filename, and returns a dict."""
    # oe_pop_dscovr_s20161013000000_e20161013235959_p20161014022626_pub.nc.gz
    file_pattern = re.compile('oe_(?P<datatype>pop|m1m|f1m)_(?P<satellite>dscovr)_s(?P<t0>\d{14})_e(?P<t1>\d{14})_p(?P<tp>\d{14})_pub\.(?P<ext>.*)')
    filename = os.path.basename(filepath)
    match = file_pattern.match(filename)
    dictout = {}
    if match:
        dictout = match.groupdict()
        dictout['t0'] = pd.to_datetime(dictout['t0'])
        dictout['t1'] = pd.to_datetime(dictout['t1'])
        dictout['tp'] = pd.to_datetime(dictout['tp'])
    dictout['filename'] = filename
    dictout['filepath'] = filepath
    return(dictout)


def data_dir_contents(data_dir):
    glob_pattern = os.path.join(data_dir, '*')
    filelist = sorted(glob(glob_pattern))
    return pd.DataFrame([match_file(file) for file in filelist], columns=['filename', 'filepath', 'satellite', 'datatype', 't0', 't1', 'tp', 'ext'])    


def timeinterval_filelist(data_dir, datatype, t0, t1):
    dir_contents = data_dir_contents(data_dir)
    files_for_type = dir_contents[dir_contents['datatype'] == datatype]
    files_for_date = files_for_type[(files_for_type['t1'] >= t0) & (files_for_type['t0'] <= t1)]
    # Need to remove files that have same t0 and t1 but older tp
    latest_versions = files_for_date.groupby(['t0','t1']).tp.transform(max)
    return files_for_date[files_for_date['tp'] == latest_versions]

def ensure_data_dir():
    data_dir = f'{STORAGE_LOCATION}/dscovr_data/'
    if not os.path.exists(data_dir):
        os.makedirs(data_dir)
    return(data_dir)

def timeinterval_to_pandas(t0, t1):
    data_dir = ensure_data_dir()
    # Download data if not yet available
    download('m1m', t0, t1)
    download('f1m', t0, t1)
    download('pop', t0, t1)
    magfiles = timeinterval_filelist(data_dir, 'm1m', t0, t1)
    fcfiles = timeinterval_filelist(data_dir, 'f1m', t0, t1)
    orbitfiles = timeinterval_filelist(data_dir, 'pop', t0, t1)
    df_dscovr_f1m = netcdftools.pandas_from_netcdf_filelist(fcfiles, t0, t1)
    df_dscovr_m1m = netcdftools.pandas_from_netcdf_filelist(magfiles, t0, t1)
    df_dscovr_orbit = netcdftools.pandas_from_netcdf_filelist(orbitfiles, t0, t1)
    df_dscovr_orbit = df_dscovr_orbit.resample('1Min').interpolate()
    df = pd.concat([df_dscovr_m1m, df_dscovr_f1m, df_dscovr_orbit],axis=1)[t0:t1]
    dscovr_columns = {'bx_gse': 'Bx',
                  'by_gse': 'By',
                  'bz_gse': 'Bz',
                  'sat_x_gse': 'Px',
                  'sat_y_gse': 'Py',
                  'sat_z_gse': 'Pz',
                  'proton_vx_gse': 'Vx',
                  'proton_vy_gse': 'Vy',
                  'proton_vz_gse': 'Vz',
                  'proton_density': 'proton_density',
                  'alpha_density': 'alpha_density'}
    df = df[list(dscovr_columns)].copy().rename(columns=dscovr_columns)
    df['proton_speed'] = np.sqrt(df['Vx']**2 + df['Vy']**2 + df['Vz']**2)
    return df
