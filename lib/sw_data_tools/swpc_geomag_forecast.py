import pandas as pd
import numpy as np
import requests
from . import basictools

def download_current_geomagnetic_forecast():
    localdir = basictools.ensure_data_dir('swpc_geomag_forecast')
    filename = f'{localdir}/3-day-geomag-forecast.txt'

    url = 'https://services.swpc.noaa.gov/text/3-day-geomag-forecast.txt'
    try:
        r = requests.get(url)
        lines = r.text.splitlines()
    except:
        return

    if lines[0] != ':Product: Geomagnetic Forecast':
        print("Geomagnetic Forecast product file expected")

    return lines


def kp_geomagnetic_forecast():
    lines = download_current_geomagnetic_forecast()
    datalinenum = 0
    data = []
    for line in lines:
        if line.startswith(':Issued:'):
            issuedate = pd.to_datetime(line[9:])
            issueyear = issuedate.year
        if line.startswith('NOAA Kp index forecast'):
            startdatestring = line[23:29]
            enddatestring = line[32:38]
            startdate = pd.to_datetime(f"{startdatestring} {issueyear}")
            enddate = pd.to_datetime(f"{enddatestring} {issueyear}")
            dtindex = pd.date_range(startdate, freq='3H', periods=3*8)
            data = np.empty(len(dtindex))
        if line[2:3] == '-' and line[5:7] == 'UT':
            linedata = line[8:].split()
            data[datalinenum] = linedata[0]
            data[datalinenum+8] = linedata[1]
            data[datalinenum+16] = linedata[2]
            datalinenum = datalinenum + 1

    df = pd.DataFrame(data={'Kp': data}, index=dtindex)
    df.index.name = 'DateTime'
    return df

#def swpc_mag():
#    import pandas as pd
#    #url = 'https://services.swpc.noaa.gov/products/solar-wind/mag-7-day.json'
#    url = 'static/data/mag-7-day.json'
#    df = pd.read_json(url, convert_dates=[0])
#    df.columns = df.loc[0]
#    df = df.drop([0], axis=0)
#    df['time_tag'] = pd.to_datetime(df['time_tag'])
#    df.index = df['time_tag']
#    df = df.drop(['time_tag'], axis=1)
#    df = df.apply(pd.to_numeric)
#    return df
