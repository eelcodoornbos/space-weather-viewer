import pandas as pd
import glob
import os
import datetime
import requests
import shutil
from . import basictools

#def realtime_file_outdated():
#    filelist = glob.glob('static/data/plasma-7-day_*.json')
#    max_file_age = pd.to_timedelta(5, 'D')
#    for path in filelist:
#        if pd.to_timedelta(datetime.datetime.now() - datetime.datetime.fromtimestamp(os.path.getmtime(path))) > max_file_age:
#            print("Realtime plasma file is not outdated")
#            return False
#    return True

def download_if_outdated(filename, maxage, url):
    """Checks if a file is older than a certain age, or does not exist, or is empty. If so, download a new copy of the file from URL.

    Parameters:
    filename (string):     name of the file on the filesystem
    maxage (pd.TimeDelta): age of the file
    url (string):          URL to download a new copy of the file from

    Returns:
    nothing
    """
    if not os.path.isfile(filename):
        age = maxage + pd.to_timedelta(1, 'D') # If the file does not exist, we make a fake age that is surely older than maxage
    else:
        statinfo = os.stat(filename)
        if statinfo.st_size <= 0:
            age = maxage + pd.to_timedelta(1, 'D') # If the file does not exist, we make a fake age that is surely older than maxage
        else:
            age = pd.to_timedelta(datetime.datetime.now() - datetime.datetime.fromtimestamp(statinfo.st_mtime))
    if age > maxage:
        r = requests.get(url)
        if r.status_code == 200:
            with open(filename, 'wb') as f:
                f.write(r.content)
            timetag = pd.Timestamp.utcnow().strftime('%Y-%m-%dT%H:%M:%S')
            (basename, ext) = os.path.splitext(filename)
            filename_timetagged = f"{basename}_{timetag}{ext}"
            shutil.copyfile(filename, filename_timetagged)            
        else:
            print("Download of {filename} from {url} has failed.")


def plasma():
    localdir = basictools.ensure_data_dir('swpc_realtime_plasma_l1')
    download_if_outdated(f'{localdir}/plasma-7-day.json', pd.to_timedelta(1, 'm'), 'https://services.swpc.noaa.gov/products/solar-wind/plasma-7-day.json')
    filelist = glob.glob(f'{localdir}/plasma-7-day*.json')
    dfs = []
    for file in filelist:
        print(f"Reading: {file}")        
        dfs.append(json_to_df(file))
    return pd.concat(dfs).sort_index().drop_duplicates()


def mag():
    localdir = basictools.ensure_data_dir('swpc_realtime_mag_l1')
    download_if_outdated(f'{localdir}/mag-7-day.json', pd.to_timedelta(1, 'm'), 'https://services.swpc.noaa.gov/products/solar-wind/mag-7-day.json')
    # Paste together the data
    filelist = glob.glob(f'{localdir}/mag-7-day*.json')
    dfs = []
    for file in filelist:
        print(f"Reading: {file}")

        dfs.append(json_to_df(file))
    return pd.concat(dfs).sort_index().drop_duplicates()


def json_to_df(file):
    '''Convert the strange SWPC realtime SW data format to a proper Pandas dataframe

    Parameters:
    file (string): name of the JSON file containing SWPC plasma data

    Returns:
    df (pandas.Dataframe): Pandas dataframe containing the data from the JSON file
    '''
    try:
        df = pd.read_json(file)
    except ValueError:
        print(f"Error parsing {file}")
        return pd.DataFrame() # Return empty dataframe on parsing error
    # Column headers are actually in first row
    df.columns = df.loc[0] 
    df = df.drop([0], axis=0) 
    # Convert time tag from string to proper time tag, and set as index
    df['time_tag'] = pd.to_datetime(df['time_tag'], utc=True)
    df.index = df['time_tag']
    df = df.drop(['time_tag'], axis=1)
    # Convert the remaining columns from string to numeric format
    df = df.apply(pd.to_numeric)
    return df


if __name__ == "__main__":
    import sys
    arguments = sys.argv[1:]
    if len(arguments) == 0:
        print("Downloading latest realtime space weather data")
        plasma()
        mag()
#    elif len(arguments) == 2:
#        with pd.option_context('display.max_rows', None, 'display.max_columns', None):
#            print(timeinterval_to_pandas(pd.to_datetime(arguments[0], utc=True), pd.to_datetime(arguments[1], utc=True)))


