import os
import numpy as np
import pandas as pd

def download_file(filename):
    import urllib.request
    filenamev4 = filename.replace('v05','v04')
    if os.path.isfile('wind_data/' + filename) or os.path.isfile('wind_data/' + filenamev4):
        return
    else:
        try:
            url = f'https://wind.nasa.gov/mission/wind/mfi/{filename}'
            print("Downloading " + url)        
            urllib.request.urlretrieve(url, 'wind_data/' + filename)
        except urllib.request.HTTPError:
            url = f'https://wind.nasa.gov/mission/wind/mfi/{filenamev4}'            
            print("Downloading " + url)        
            urllib.request.urlretrieve(url, 'wind_data/' + filenamev4)
    
def filelist(t0, t1):
    range_days = pd.date_range(t0,t1)
    filelist = []
    for day in range_days:
        filenamev5 = f'wi_h0_mfi_{day.strftime("%Y%m%d")}_v05.cdf'
        filenamev4 = f'wi_h0_mfi_{day.strftime("%Y%m%d")}_v04.cdf'
        if os.path.isfile(f'wind_data/{filenamev5}'):
            filelist.append(filenamev5)
        elif os.path.isfile(f'wind_data/{filenamev4}'):
            filelist.append(filenamev4)
        else:
            download_file(filenamev5)
    return filelist

def file_to_pandas(filename):
    from spacepy import pycdf
    cdf = pycdf.CDF(filename)
    timeindex = pd.DatetimeIndex(cdf['Epoch'][:,0])
    xyzfields = ['BGSE', 'PGSE']
    dataseries = []
    axisdict = {0: 'X', 1: 'Y', 2: 'Z'}
    for field in xyzfields:
        for axisnum in np.arange(0,3):
            newseries = pd.Series(data = cdf[field][:,axisnum], index=timeindex, name = field + '_' + axisdict[axisnum])
            newseries[(newseries == cdf[field].attrs['FILLVAL'])] = np.nan
            dataseries.append(newseries)

    df = pd.concat(dataseries, axis=1)
    
    return df

def timerange_to_pandas(t0, t1):
    listoffiles = filelist(t0, t1)
    dfs = []
    for filename in listoffiles:
        path = f'wind_data/{filename}'
        dfs.append(file_to_pandas(path))
    df = pd.concat(dfs)[t0:t1]

    # Simplify column names
    wind_columns = {'BGSE_X': 'Bx',
                    'BGSE_Y': 'By',
                    'BGSE_Z': 'Bz',
                    'PGSE_X': 'Px',
                    'PGSE_Y': 'Py',
                    'PGSE_Z': 'Pz'}
    df = df[list(wind_columns)].copy().rename(columns=wind_columns)    

    # Change units from Earth-radii to km
    Re = 6378.0
    df['Px'] = df['Px'] * Re
    df['Py'] = df['Py'] * Re
    df['Pz'] = df['Pz'] * Re    
    return df


