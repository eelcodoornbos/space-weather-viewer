#!/usr/bin/env python3
import os
STORAGE_LOCATION='.'
def ensure_data_dir(type):
    directory = f'{STORAGE_LOCATION}/{type}'
    if not os.path.exists(directory):
        os.makedirs(directory)
    return directory
