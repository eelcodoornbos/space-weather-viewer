import gzip
import pandas as pd
import numpy as np
from tempfile import NamedTemporaryFile
from netCDF4 import Dataset
        
def pandas_from_netcdf_file(filepath):
    # Check if the file is compressed
    zipped = (filepath[-3:] == '.gz')
    if zipped:
        with gzip.open(filepath, 'rb') as gzip_fh:
            file_content = gzip_fh.read()
        netcdf_fh = NamedTemporaryFile()
        netcdf_fh.write(file_content)
        netcdf_fh.flush()
        netcdf_filename = netcdf_fh.name
    else:
        netcdf_filename = filepath
        
    # Open the NetCDF data file and DataFrame objects
    rootgrp = Dataset(netcdf_filename, "r", format="NETCDF4")
    datetimeindex = pd.DatetimeIndex(pd.to_datetime(rootgrp['time'][:], unit='ms'))
    df = pd.DataFrame(index = datetimeindex)
    
    # Read the data from the NetCDF variables
    for var in rootgrp.variables.keys():
        dims = rootgrp[var].dimensions
        if (len(dims) == 1) & (dims[0] == 'time'):
            df[var] = rootgrp[var][:]
            if 'missing_value' in rootgrp[var].ncattrs():
                df[var] = df[var].replace({rootgrp[var].missing_value: np.nan})
    
    if zipped: # close the temporary uncompressed file to remove it
        netcdf_fh.close() 

    return df


def pandas_from_netcdf_filelist(filelist,t0, t1):
    df_list = []
    for file in filelist.itertuples():
        df = pandas_from_netcdf_file(file.filepath)
        if not df.empty:
            df_list.append(df)
    return pd.concat(df_list)
