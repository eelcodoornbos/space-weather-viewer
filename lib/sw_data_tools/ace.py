import pandas as pd
import numpy as np
from . import basictools

def download_mag_file(year):
    '''Download an ACE 16-second magnetometer data file from the CalTech server for a specific year'''
    import sys
    import shutil
    import ftplib
    import os.path
    ftp_host = 'mussel.srl.caltech.edu'
    ftp_dir = '/pub/ace/level2/mag/'
    ftp_filename = f'mag_data_16sec_year{year}.hdf'
    localdir = basictools.ensure_data_dir('ace')
    ftp_localfilename = f'{localdir}/{ftp_filename}'
    h5filename = f'{localdir}/mag_data_16sec_year{year}.h5'
    if not os.path.isfile(ftp_localfilename): 
        print(f"Attempting to download {ftp_localfilename} from {ftp_host + ftp_dir + ftp_filename}")
        ftp_connection = ftplib.FTP(ftp_host)
        ftp_connection.set_pasv(False)
        ftp_connection.login()
        ftp_connection.cwd(ftp_dir)
        try:
            ftp_connection.retrbinary("RETR " + ftp_filename, open(ftp_localfilename, 'wb').write)
        except ftplib.all_errors as e:
            print(f"Error trying to download file {ftp_filename}: + {e}")
            os.remove(ftp_localfilename)
        ftp_connection.quit()
    if not os.path.isfile(h5filename):
        print(f"Converting {ftp_localfilename} to HDF5 format")
        if shutil.which('h4toh5'): 
            os.system(f"h4toh5 {ftp_localfilename} {h5filename}")
        else:
            print("The h4toh5 executable was not found")
            sys.exit()
    else:
        print(f"File {h5filename} already exists")

        
def download_swepam_file(year):
    '''Download an ACE SWEPAM data file from the CalTech server for a specific year'''
    import ftplib
    import os.path
    ftp_host = 'mussel.srl.caltech.edu'
    ftp_dir = '/pub/ace/level2/swepam/'
    ftp_filename = f'swepam_data_64sec_year{year}.hdf'
    localdir = basictools.ensure_data_dir('ace')
    ftp_localfilename = f'{localdir}/{ftp_filename}'
    h5filename = f"{localdir}/swepam_data_64sec_year{year}.h5"
    if not os.path.isfile(ftp_localfilename) or os.path.isfile(h5filename):
        ftp_connection = ftplib.FTP(ftp_host)
        ftp_connection.login()
        ftp_connection.cwd(ftp_dir)
        ftp_connection.retrbinary("RETR " + ftp_filename, open(ftp_localfilename, 'wb').write)
        ftp_connection.quit()
    if not os.path.isfile(h5filename):
        print(f"Converting {ftp_localfilename} to HDF5 format")      
        os.system(f"h4toh5 {ftp_localfilename} {h5filename}")
    else:
        print(f"File {ftp_localfilename} already exists")


def compose_date(years, months=1, days=1, weeks=None, hours=None, minutes=None,
                 seconds=None, milliseconds=None, microseconds=None, nanoseconds=None):
    '''Returns array of numpy datetime64 objects, as a function of calendar date components'''
    years = np.asarray(years) - 1970
    months = np.asarray(months) - 1
    days = np.asarray(days) - 1
    types = ('<M8[Y]', '<m8[M]', '<m8[D]', '<m8[W]', '<m8[h]',
             '<m8[m]', '<m8[s]', '<m8[ms]', '<m8[us]', '<m8[ns]')
    vals = (years, months, days, weeks, hours, minutes, seconds,
            milliseconds, microseconds, nanoseconds)
    return sum(np.asarray(v, dtype=t) for t, v in zip(types, vals)
               if v is not None)


def mag_file_to_pandas(filename):
    store = pd.io.pytables.HDFStore(filename)
    df_ace = store['/VG_MAG_data_16sec/MAG_data_16sec'].replace(-9999.900391,np.NaN)
    df_ace.index = compose_date(df_ace['year'], days=df_ace['day'], hours=df_ace['hr'], minutes=df_ace['min'], seconds=df_ace['sec'])
    # Remove invalid data
    df_ace.loc[df_ace['Bgse_x'] < -990, 'Bgse_x'] = np.nan
    df_ace.loc[df_ace['Bgse_y'] < -990, 'Bgse_y'] = np.nan
    df_ace.loc[df_ace['Bgse_z'] < -990, 'Bgse_z'] = np.nan
    df_ace.loc[df_ace['Bgsm_x'] < -990, 'Bgsm_x'] = np.nan
    df_ace.loc[df_ace['Bgsm_y'] < -990, 'Bgsm_y'] = np.nan
    df_ace.loc[df_ace['Bgsm_z'] < -990, 'Bgsm_z'] = np.nan
    df_ace.index.name = "time_tag"
    store.close()
    return df_ace


def swepam_file_to_pandas(filename):
    store = pd.io.pytables.HDFStore(filename)
    df_ace = store['/VG_SWEPAM_ion/SWEPAM_ion'].replace(-9999.900391,np.NaN)
    df_ace.index = compose_date(df_ace['year'], days=df_ace['day'], hours=df_ace['hr'], minutes=df_ace['min'], seconds=df_ace['sec'] )
    df_ace.index.name = "time_tag"
    store.close()
    return df_ace

    
def mag_timeinterval_to_pandas(t0, t1):
    localdir = basictools.ensure_data_dir('ace')
    year0 = t0.strftime("%Y")
    year1 = t1.strftime("%Y")
    yearrange = pd.date_range(start=pd.to_datetime(year0), end=pd.to_datetime(year1), freq='AS')
    dfs = []
    for year in yearrange:
        download_mag_file(year.strftime('%Y'))
        ace_filename = f'{localdir}/mag_data_16sec_year{year.strftime("%Y")}.h5'
        print("Reading " + ace_filename)
        dfs.append(mag_file_to_pandas(ace_filename))
    df = pd.concat(dfs)[t0:t1]
    ace_columns = {'Bgsm_x': 'bx_gsm',
                   'Bgsm_y': 'by_gsm',
                   'Bgsm_z': 'bz_gsm',
                   'pos_gse_x': 'Px',
                   'pos_gse_y': 'Py',
                   'pos_gse_z': 'Pz'}
    df = df[list(ace_columns)].copy().rename(columns=ace_columns)
    df['bt'] = np.sqrt(df.bx_gsm**2 + df.by_gsm**2 + df.bz_gsm**2) 
    return df

def swepam_timeinterval_to_pandas(t0, t1):
    year0 = t0.strftime("%Y")
    year1 = t1.strftime("%Y")
    yearrange = pd.date_range(start=pd.to_datetime(year0), end=pd.to_datetime(year1), freq='AS')
    dfs = []
    localdir = basictools.ensure_data_dir('ace')
    for year in yearrange:
        download_swepam_file(year.strftime('%Y'))
        ace_filename = f'{localdir}/swepam_data_64sec_year{year.strftime("%Y")}.h5'
        print("Reading " + ace_filename)
        dfs.append(swepam_file_to_pandas(ace_filename))
    df = pd.concat(dfs)[t0:t1]
    ace_columns = {'proton_density': 'density', 
                   'proton_temp': 'temperature', 
                   'He4toprotons': 'He4toprotons', 
                   'proton_speed': 'speed',
                   'x_dot_GSE': 'x_dot_GSE', 
                   'y_dot_GSE': 'y_dot_GSE', 
                   'z_dot_GSE': 'z_dot_GSE', 
                   'x_dot_RTN': 'x_dot_RTN', 
                   'y_dot_RTN': 'y_dot_RTN',
                   'z_dot_RTN': 'z_dot_RTN', 
                   'x_dot_GSM': 'x_dot_GSM', 
                   'y_dot_GSM': 'y_dot_GSM', 
                   'z_dot_GSM': 'z_dot_GSM', 
                   'pos_gse_x': 'Px',
                   'pos_gse_y': 'Py', 
                   'pos_gse_z': 'Pz'}
    df = df[list(ace_columns)].copy().rename(columns=ace_columns)    
    return df
