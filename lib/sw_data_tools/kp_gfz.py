#!/usr/bin/env python3
import os
import datetime
import pandas as pd
from . import basictools

def download_wdc_file(date):
    '''Download a Kp WDC data file from the GFZ-Potsdam FTP server for a specific year'''
    import ftplib
    localdir = basictools.ensure_data_dir('kp_gfz')
    ftp_host = 'ftp.gfz-potsdam.de'
    ftp_dir = '/pub/home/obs/kp-ap/wdc/yearly'
    ftp_filename = f"kp{date.strftime('%Y')}.wdc"
    ftp_localfilename = f'{localdir}/{ftp_filename}'

    if os.path.isfile(ftp_localfilename):
        file_date = datetime.datetime.fromtimestamp(os.path.getmtime(ftp_localfilename))
        now_date = datetime.datetime.now()
        file_age = pd.to_timedelta(now_date - file_date)
        max_file_age = pd.to_timedelta(15, 'm')
        if  file_age > max_file_age:
            download = True
        else:
            download = False # Has recently been downloaded
    else:
       download = True

    if download:
        print(f"Downloading Kp file: {ftp_filename}")
        ftp_connection = ftplib.FTP(ftp_host)
        ftp_connection.login()
        ftp_connection.cwd(ftp_dir)
        try:
            print(f"Attempting download of {ftp_localfilename} from server {ftp_host} in directory {ftp_dir}")
            ftp_connection.retrbinary("RETR " + ftp_filename, open(ftp_localfilename, 'wb').write)
        except ftplib.all_errors as e:
            print(f"Error trying to download file {ftp_filename}: {e}")
            os.remove(ftp_localfilename)
        ftp_connection.quit()


def download_ql_wdc_file():
    '''Download the quick-look Kp WDC data files from the GFZ-Potsdam website'''
    import requests
    from shutil import copy2
    files = ['pqlyymm.wdc', 'qlyymm.wdc']
    max_file_ages = [pd.to_timedelta(15, 'm'), pd.to_timedelta(5, 'm')]
    localdir = basictools.ensure_data_dir('kp_gfz')
    for filename, max_file_age in zip(files,max_file_ages):
        localfilename = f'{localdir}/{filename}'
        download = False
        if os.path.isfile(localfilename):
            if pd.to_timedelta(datetime.datetime.now() - datetime.datetime.fromtimestamp(os.path.getmtime(localfilename))) > max_file_age:
                download = True
            else:
                download = False
        else:
            download = True
        if download:
            url = f'http://www-app3.gfz-potsdam.de/kp_index/{filename}'
            print(f"Attempting download of {filename}")
            try:
                r = requests.get(url, allow_redirects=False)
                r.raise_for_status()
                open(localfilename, 'wb').write(r.content)
                yymm = r.content[0:4].decode('ascii').replace(' ','0') # yymm
                archivefilename = localfilename.replace('yymm',yymm)
                print(f"Copying {localfilename} to {archivefilename}")
                copy2(localfilename, archivefilename)
            except Exception as err:
                print(err)

def file_to_pandas(filename):
    '''Parse a Kp WDC data file and return as a Pandas dataframe'''
    # Need to get first two digits of 4-digit year from filename, to prevent time conversion error
    basename = os.path.basename(filename)
    if (basename[0:2] == 'pq' or basename[0:2] == 'ql'):
        century_from_filename = '20'
    else:
        century_from_filename = basename[2:4]
    # Set up
    threehour = pd.to_timedelta(3, 'H')
    starttimes = []
    endtimes   = []
    kpvalues   = []
    with open(filename) as f:  
        for cnt, kpline in enumerate(f):
            date = pd.to_datetime(datetime.datetime.strptime(century_from_filename + kpline[0:6].replace(" ", "0"), '%Y%m%d'), utc=True)
            for interval in range(0,8):
                starttimes.append(date + interval * threehour)
                endtimes.append(date + (interval+1) * threehour)
                kpvalues.append(float(kpline[12+interval*2:12+(interval+1)*2])/10)

    df = pd.DataFrame(index=starttimes, data={"EndTime": endtimes, "Kp": kpvalues})
    df.index.name = "DateTime"
    return df
    
def timeinterval_to_pandas(t0, t1):
    '''For a certain time interval, return a Pandas dataframe with contents 
    from a set of monthly Kp WDC files.'''
    localdir = basictools.ensure_data_dir('kp_gfz')
    yearrange = pd.period_range(start=t0, end=t1, freq='Y')
    dfs = []
    for year in yearrange:
        # The file for next year will not yet exist
        if year.year < pd.Timestamp.utcnow().year + 1:
            download_wdc_file(year)
            filename = f"kp{year.strftime('%Y')}.wdc"
            filepath = f'{localdir}/{filename}'
            dfs.append(file_to_pandas(filepath))

    df = pd.concat(dfs)

    dfs = [df] 

    # Check if quicklook data should be appended
    tlast = df.index[-1] + pd.to_timedelta(3,'H') # End of validity of final data
    if t1 > tlast:
        print("Need quicklook Kp data")
        download_ql_wdc_file()
        df_ql = quicklook_to_pandas()[tlast:] # Discard any quicklook data that overlaps with final data
        dfs.append(df_ql) 

    df = pd.concat(dfs)
    return df[t0:t1].query('Kp != 9.9')

def quicklook_to_pandas():
    localdir = basictools.ensure_data_dir('kp_gfz')
    now = pd.Timestamp.utcnow()
    lastmonth = now.replace(day=1) - pd.to_timedelta('1D')
    yymm_now = now.strftime('%y%m')
    yymm_lastmonth = lastmonth.strftime('%y%m')
    print(f"{yymm_now} / {yymm_lastmonth}")
    dfpql = file_to_pandas(f'{localdir}/pql{yymm_lastmonth}.wdc')
    dfql = file_to_pandas(f'{localdir}/ql{yymm_now}.wdc')
    df = pd.concat([dfpql, dfql]).query('Kp != 9.9')
#    df = file_to_pandas(f'{localdir}/qlyymm.wdc').query('Kp != 9.9')
    return df

if __name__ == "__main__":
    import sys
    arguments = sys.argv[1:]
    if len(arguments) == 0:
        print("Downloading latest Kp data")
        download_wdc_file(pd.Timestamp.now()) 
        download_ql_wdc_file()
    elif arguments[0] == 'ql':
        download_ql_wdc_file()
        with pd.option_context('display.max_rows', None, 'display.max_columns', None):
            print(quicklook_to_pandas())
    elif len(arguments) == 2:
        with pd.option_context('display.max_rows', None, 'display.max_columns', None):
            print(timeinterval_to_pandas(pd.to_datetime(arguments[0], utc=True), pd.to_datetime(arguments[1], utc=True)))
