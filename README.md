# Space Weather Viewer
Space Weather Viewer is a web-based application for visualizing space weather data. It focuses on a clean and crisp display of information, highlighting relationships between various data sources, and providing interactivity through smooth zooming and panning on time series data.

The software includes a back-end written in Python (version 3.6 or higher). This back-end includes a data store and a web server, which serves the data, HTML, JavaScript and CSS for the front-end, which is then accessible from any web browser.

## Requirements
To run the back end, the following external Python packages need to be installed (for example via [pip](https://pypi.org/project/pip/) or [conda](https://docs.conda.io/en/latest/)):
* [numpy](https://numpy.org)
* [Pandas](https://pandas.pydata.org)
* [Flask](https://flask.palletsprojects.com/)

Depending on the needs for external data formats that are to be used, the following packages need to be installed as well:
* [NetCDF4](https://unidata.github.io/netcdf4-python/netCDF4/index.html) - for reading NetCDF files
* [spacepy](https://spacepy.github.io) - for reading NASA CDF files

## Running space weather viewer
With these packages installed, the back-end server can be started from the directory in which this project has been cloned, by running the following command in the Terminal:

    python sw_viewer.py
    
This should start up the Flask server, which will provide a URL, through which the front-end can be accessed in a web browser.
